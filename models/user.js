let mongoose = require("mongoose");
let passportLocalMongoose = require("passport-local-mongoose");

let UserSchema = new mongoose.Schema({
    username : String,
    password : String,
    isAdmin : Boolean
});

UserSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("user", UserSchema);
# GuvennliYazilimGelistirme

Güvenli Yazılım geliştirme Arasınav projesi için oluşturulmuştur.

Web uygulaması backend olarak nodejs kullanmaktadır. 
Ubuntu server üzerinde aşağıdaki komut ile nodejs kurulmaktadır.

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

Web uygulaması veritabanı olarak mongodb kullanmaktadır.
apt-get install mongodb

Web uygulaması reposu indirmesi ile uygulama ana klasoru içerisinde aşağıdaki komut çalıştırılarak dependencies indirilir.
npm install

Web uygulaması aşağıdaki komut ile web sunucusu içerisinde çalıştırılmaya başlanılır.
node app.js


var express               = require("express"),
    mongoose              = require("mongoose"),
    passport              = require("passport"),
    bodyParser            = require("body-parser"),
    User                  = require("./models/user"),
    LocalStrategy         = require("passport-local");
   passportLocalMongoose = require("passport-local-mongoose");
var path = require('path');
var cmd=require('node-cmd');
var st=require('st');
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/auth_demo_app");

let app = express();
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(st({path: './gizli', url: '/gizli'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(require("express-session")({
    secret: "cok gizli bir kelime",
    resave: false,
    saveUninitialized : true
}));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


/// linux komutlarının tanımı icin oluşturuldu

var bilgiSchema = new mongoose.Schema({
  komut: String,
  tanim: String,
});
var bilgi =mongoose.model('bilgi', bilgiSchema);

var favoriSchema = new mongoose.Schema({
  user: String,
  komut: String,
});
var favori =mongoose.model('favori', favoriSchema);




//============
//ROUTES
//============


/////komut arama


var komut = [];
//var out = [];
app.get("/ogren",isLoggedIn, function(req, res){

    res.render("ogren", {komut: komut
    });

});


app.post("/get-ogren-komut", function(req, res){
    komut = req.body.komutogren;
    bilgi.findOne({"komut":komut } ,function(err, out){
        console.log(out);
        console.log(komut);
        console.log(JSON.stringify(out));
        komut = JSON.stringify(out);
        komut=komut.split(",");
        console.log(komut);
        //res.send(out);
        res.render("ogren", {komut : komut});
    });
});

app.post("/get-ogren-favori", function(req, res){
        var favkomut = req.body.favori;
        let addfavori = new favori ({
        user : req.user.username,
        komut: favkomut });
        addfavori.save(function (err,favori){
        if(err) {
        console.log("veritabanina eklemede sorun")
    } else {
        console.log("basarili eklendi");
        console.log(favori);
    }
});
        res.redirect("ogren");

});


/////// komut arama son

var ip = [];
app.get("/run-cmd", isLoggedIn, function(req, res){
    res.render("run-cmd", {ip:ip});
});
app.post("/get-cmd-command", function(req, res){
    ip = req.body.ipadr;
    cmd.get(
         ip,
        function(err, data, stderr){
            console.log(data);
            ip = data.split("\n");
            res.render("run-cmd", {ip:ip});
        }
    );
});

// kullanıcı yetki kısıtlama https://access.redhat.com/solutions/65822

app.get("/", function(req, res) {
    res.render("home");
});


app.get("/secret", isLoggedIn, function(req, res){
    res.render("secret");
});


var comments = [ "Güzel bir site"];

app.post("/addComment",isLoggedIn, function(req, res){
    var newComment  = req.body.newComment;
    comments.push(newComment);
    res.redirect("/comments");
});

app.get("/comments", function(req, res){
    res.render("comments", {comments:comments});
}); 
////////////////////////////

// Auth Routes

//show sign up form

app.get("/register", function(req, res){
    res.render("register");
});


//handling user signup
app.post("/register", function(req, res){
    req.body.username
    req.body.password
    req.body.isAdmin
    User.register(new User({ username: req.body.username, isAdmin: req.body.isAdmin  }), req.body.password, function(err, user){
        if(err){
            console.log(err);
            return res.render('register');
        }
        passport.authenticate("local")(req, res, function(){
            res.redirect("/secret");
        });
    });
});

///// ADMIN AREA!!!! ////////

app.get("/addkomut",isLoggedIn, function(req, res){
    var loginuser= req.user.username;
    var adminUser;
    User.findOne({"isAdmin": true } ,function(err, out){
    adminUser = out["username"];
    res.render("addkomut", {loginuser:loginuser, adminUser:adminUser} );
        //res.send(out);
    });
});


app.post("/addkomut-get", function(req, res) {
    var myData = new bilgi(req.body);
    myData.save()
        .then(item => {
            res.redirect("addkomut");
        })
        .catch(err => {
            res.status(400).send("Veritabanina eklemede bir hata olustu");
        });
});

app.post("/addkomut-update", function(req, res, next){
    var bulkomut = req.body.komut;
    var degistanim = req.body.tanim;

    bilgi.update({ "komut": bulkomut } , 
    { $set: { "komut": bulkomut, "tanim": degistanim } }, function(err, out){

        console.log(out);

        //res.send(out);
        res.redirect("addkomut");
    });
});

app.post("/addkomut-delete", function(req, res, next){
    var bulkomut = req.body.komut;
    bilgi.deleteOne({ "komut": bulkomut }, function(err, out){
        console.log(out);
        res.redirect("addkomut");
    });
});

///// ADMIN AREA!!!! END ////////

//LOGIN ROUTES
//render login form
app.get("/login", function(req, res){
    res.render("login");
});
//login logic
//middleware
app.post("/login",passport.authenticate("local", {
    successRedirect: "/secret",
    failureRedirect: "/login"
}), function(req, res){
});

app.get("/logout",function(req, res){
   req.logout();
   res.redirect("/");
});

function isLoggedIn(req, res, next) {
    if( req.isAuthenticated() ){
        return next();
    }
    res.redirect("/login");
}
app.listen(3001, "10.107.5.131", function() {
    console.log("server started...");
});
